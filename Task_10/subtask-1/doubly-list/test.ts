import { DoublyLinkedList } from "./classes/doubly-list";

const list = new DoublyLinkedList();

list.simpleAddNode(0);
list.simpleAddNode(1);
list.simpleAddNode(2);
list.simpleAddNode(3);
list.addNode(4, 100);
list.edit(0, 777);
list.delete(1);

list.infoNode(0);
list.infoNode(1);
list.infoNode(2);
list.infoNode(3);
