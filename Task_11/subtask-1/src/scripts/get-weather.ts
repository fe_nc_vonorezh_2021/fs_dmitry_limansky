import { insertCurrentWeather } from "./insert-current-weather";
import { insertWeatherForThreeDays } from "./insert-three-days-weather";
import { insertInformationBlock } from "./insert-info-block";
import { delayMessage } from "./delay";

export async function getWeather(param: string, defaultCity: string): Promise<any> {
    const cityName: string = defaultCity === "" ? (<HTMLInputElement>document.getElementById("city_id")).value : defaultCity;
    let response = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${cityName}&lang=ru&appid=87a4e3b57fd5877a435275e9fae66ccd`);
    let data = await response.json();
    if (response.ok) {
        const lon: string = data.coord.lon.toFixed(2);
        const lat: string = data.coord.lat.toFixed(2);
        let hourlyResponse = await fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=minutely&appid=87a4e3b57fd5877a435275e9fae66ccd`);
        let hourlyData = await hourlyResponse.json();
        insertInformationBlock(data);
        if (param === "current") {
            insertCurrentWeather(hourlyData);
        } else {
            insertWeatherForThreeDays(hourlyData);
        }
    }
    else {
        delayMessage();
    }
}
