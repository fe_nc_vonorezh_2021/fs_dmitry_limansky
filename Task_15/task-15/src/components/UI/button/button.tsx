import React, { FC, ReactChild, ReactNode } from 'react';
import classes from './button.module.css';

interface ChildProps {
    children: ReactChild | ReactNode;
    onClick() : void;
  } 
 
const Button:FC<ChildProps> = ({children, onClick}) => {
    return (
        <button className={classes.mainButton} onClick={onClick}>
            {children}
        </button>
    );
};

export default Button;