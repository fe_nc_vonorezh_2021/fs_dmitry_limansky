const {fromEvent, merge} = require("rxjs");

const firstButton: any = document.getElementById("button-1");
const secondButton: any = document.getElementById("button-2");
const thirdButton: any = document.getElementById("button-3");

const bacgroundPictures: string[] =  ["url(https://wallpapershome.ru/images/pages/pic_h/20871.jpg)",
"url(https://wallpapershome.ru/images/wallpapers/apple-pro-display-xdr-2560x1440-21619.jpg)",
"url(https://searchthisweb.com/wallpaper/thumb1000/main1000_macos-mojave_5120x2880_mzvij.jpg)",
"url(https://wallpapershome.ru/images/wallpapers/makbuk-pro-2020-2560x1440-makbuk-pro-2020-23089.jpg)",
"url(https://i.pinimg.com/originals/17/7c/9a/177c9a09f61b1f7d939282c283217023.jpg)",
"url(https://i.pinimg.com/originals/54/c3/40/54c34004a92804c8cebd2bd29790f781.jpg)"];

const firstStream$: any = fromEvent(firstButton, 'click');
const secondStream$: any = fromEvent(secondButton, 'click');
const thirdStream$: any = fromEvent(thirdButton, 'click');
const buttonsStream$: any = merge(firstStream$, secondStream$, thirdStream$);
console.log("DSDSDS");

buttonsStream$.subscribe(
    () => {
        let randomElement: string = bacgroundPictures[Math.floor(Math.random() * bacgroundPictures.length)];
        document.body.style.backgroundImage = randomElement;
        console.log("Clicked!");
    }
);
