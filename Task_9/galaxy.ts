import {Planet} from "./planet";
import { Star } from "./star";

export class Galaxy {
    public name: string;
    public planetList: Planet[];
    public starList: Star[];
    constructor(name: string, planetList: Planet[], starList: Star[]) {
        this.name = name;
        this.planetList = planetList;
        this.starList = starList;
    }

    public isAlive(): void {
        let isAlive: boolean = false;
        this.planetList.forEach(element => {
            if (element.getAlive()) {
                isAlive = true;
                return;
            }
        });
        isAlive ? console.log("Данная галактика обитаема, берегись инопланетян!") : console.log("Данная галактика необитаема, инопланетян нет, но скоро появятся :(");
    }
}
