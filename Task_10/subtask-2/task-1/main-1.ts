import { Observable, interval, range } from 'rxjs';
import { filter, take, tap } from 'rxjs/operators'

const stream$: Observable<number> = range(1, 100)
    .pipe(
        tap( (v : number)=> console.log("Tap:", v)),
        filter((v : number) => isPrimeNumber(v))
    );

stream$.subscribe({
    next: (v : number) => console.log("This is a prime value: ", v),
    complete: () => console.log("Stream is complete!")
}
)

function isPrimeNumber(value: number): boolean {
    if (value <= 2) {
        return false;
    }
    let check: boolean = true;
    for (let i: number = 2; i < value; i++) {
        if (value % i === 0) {
            check = false;
            break;
        }
    }
    return check;
}
