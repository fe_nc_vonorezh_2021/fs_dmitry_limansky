import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { PetType, PetSex } from '../pets/enums';
import { PetService } from '../service/pet.service';
import { ActivatedRoute } from '@angular/router'
import { Subscription } from 'rxjs'
import { Pet } from '../pets/pet';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-edit',
  templateUrl: './form-edit.component.html',
  styleUrls: ['./form-edit.component.less']
})
export class FormEditComponent implements OnInit {

  public updateForm: FormGroup;
  private id: number;
  public currentPet: Pet;
  private subscription: Subscription;

  types = [PetType.Cat, PetType.Dog, PetType.Hamster,
  PetType.Mouse, PetType.Parrot, PetType.Rabbit];

  sexType = [PetSex.Female, PetSex.Male];

  constructor(private fb: FormBuilder, private service: PetService, private router: Router, private activateRoute: ActivatedRoute) {
    this.subscription = activateRoute.params.subscribe(
      (params) => (this.id = params['id'])
    );
  };

  ngOnInit(): void {
    this.getCurrentPet();
  }

  getCurrentPet(): void {
    this.service.getPet(this.id)
      .subscribe(animal => {
        this.currentPet = animal;
        this.initializeForm(); 
      }); 
  }

  initializeForm(): void {
    this.updateForm = this.fb.group({
      name: [this.currentPet.name, [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
      weight: [this.currentPet.weight, [Validators.required, Validators.minLength(1), Validators.maxLength(5)]],
      type: [this.currentPet.type, [Validators.required]],
      sex: [this.currentPet.sex, [Validators.required]]
    })
  }
  
  public editPet(): void {
    this.currentPet.name = this.updateForm.controls["name"].value;
    this.currentPet.weight = this.updateForm.controls["weight"].value;
    this.currentPet.type = this.updateForm.controls["type"].value;
    this.currentPet.sex = this.updateForm.controls["sex"].value;
    this.currentPet.isHidden = false;
    if (this.currentPet.type === PetType.Parrot) {
      this.currentPet.canFly = true;
    }
    this.service.updatePet(this.currentPet, this.id)
        .subscribe(res => {
          alert("Pet edited successfully");
        },
          err => {
            alert("Wrong");
          });
    this.router.navigate(["pets"]);
  }

}
