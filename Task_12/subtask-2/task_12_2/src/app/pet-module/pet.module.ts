import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PetListComponent } from './pet-list/pet-list.component';
import { InfoPetComponent } from './info-pet/info-pet.component';

@NgModule({
  declarations: [PetListComponent, InfoPetComponent],
  imports: [CommonModule ],
  exports: [PetListComponent, InfoPetComponent]
})
export class PetModule { }
