import { Observable, interval, range } from 'rxjs';
import { map, take } from 'rxjs/operators';

const stream$: Observable<number> = range (0, 6)
    .pipe(
        map((v: number) => {
            if (v > 4 ) {
                throw new Error("The countdown has gone beyond");
            } else {
                return -1 * v + 5;
            }
        }
        )
    );

stream$.subscribe({
    next: (v: number) => console.log("Current value is: ", v),
    error: () => console.log("Got an error!")
});
