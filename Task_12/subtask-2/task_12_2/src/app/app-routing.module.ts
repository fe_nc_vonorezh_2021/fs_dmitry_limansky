import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PetListComponent } from './pet-module/pet-list/pet-list.component';
import { FormAddComponent } from './form-add/form-add.component';
import { FormEditComponent } from './form-edit/form-edit.component';

const routes: Routes = [
  { path: "pets", component: PetListComponent },
  { path: "", redirectTo: "pets", pathMatch: "full" },
  { path: "addForm", component: FormAddComponent },
  { path: "pet/:id", component: FormEditComponent }
];

const childRoutes: Routes = [
  { path: "pets", component: FormAddComponent}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forRoot(routes), RouterModule.forChild(childRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
