import { Planet } from "./planet";
import { Star } from "./star";
export type NearestObject = Planet[] | Star[];
