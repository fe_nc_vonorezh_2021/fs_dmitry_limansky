import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pet } from 'src/app/pets/pet';

@Component({
  selector: 'app-info-pet',
  templateUrl: './info-pet.component.html',
  styleUrls: ['./info-pet.component.less']
})
export class InfoPetComponent implements OnInit {

  @Input()
  public pet: Pet;

  @Output()
  canSaySomething: EventEmitter<string> = new EventEmitter();

  ngOnInit(): void {
    
  } 

 saySomething(): void { 
   this.canSaySomething.emit(this.pet.name);
 }
}
