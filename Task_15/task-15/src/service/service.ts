import { Pet } from "../pets/pet";

export class AppService {
    private petList: Pet[] = [
        new Pet(1, "Dunkey", "Dog", "Male", 45, false, false),
        new Pet(2, "Musya", "Cat", "Female", 6, false, false),
        new Pet(3, "Rich", "Dog", "Male", 34, false, false),
        new Pet(4, "Homa", "Hamster", "Male", 0.5, false, false),
        new Pet(5, "Kesha", "Parrot", "Male", 1.1, true, false),
        new Pet(6, "Max", "Cat", "Male", 8, false, false),
        new Pet(7, "Jerry", "Mouse", "Male", 0.3, false, false),
        new Pet(8, "Tom", "Cat", "Male", 10, false, false),
        new Pet(9, "Rabby", "Rabbit", "Male", 1.7, false, false),
        new Pet(10, "Vasya", "Cat", "Male", 7.77, false, false),
    
    ];

    public getPetList() : Pet[] {
        return this.petList;
    }
}