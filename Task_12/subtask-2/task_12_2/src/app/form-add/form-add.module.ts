import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormAddComponent } from './form-add.component';



@NgModule({
  declarations: [FormAddComponent],
  imports: [
    CommonModule, FormsModule
  ],
  exports: [FormAddComponent]
})
export class FormAddModule { }
