import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PetModule } from './pet-module/pet.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormAddModule } from './form-add/form-add.module';
import { AppRoutingModule } from './app-routing.module';
import { FormEditModule } from './form-edit/form-edit.module';
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, PetModule, CommonModule, FormsModule, FormAddModule, AppRoutingModule, ReactiveFormsModule, FormEditModule, HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
