import { Component, OnInit } from '@angular/core';
import { Pet } from '../pets/pet';
import { PetService } from '../service/pet.service';
import { PetType } from '../pets/enums';
import { PetSex } from '../pets/enums';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-add',
  templateUrl: './form-add.component.html',
  styleUrls: ['./form-add.component.less']
})
export class FormAddComponent implements OnInit {

  constructor(private service: PetService, private router: Router) { }

  types = [PetType.Cat, PetType.Dog, PetType.Hamster,
  PetType.Mouse, PetType.Parrot, PetType.Rabbit];

  sexType = [PetSex.Female, PetSex.Male];

  public formPet: Pet = new Pet(1, "", PetType.Cat, PetSex.Female, 1, false, false);

  public addPet(): void {
    if (this.formPet.type === PetType.Parrot) {
      this.formPet.canFly = true;
    }
    let newPet: Pet = new Pet(1000, this.formPet.name, this.formPet.type, 
      this.formPet.sex, this.formPet.weight, this.formPet.canFly, this.formPet.isHidden);
    
      this.service.insertPet(newPet)
      .subscribe(res => {
        alert("Pet added successfully");
      },
        err => {
          alert("Wrong");
        });
    this.router.navigate(["pets"]);
  }

  ngOnInit(): void {
  }

}
