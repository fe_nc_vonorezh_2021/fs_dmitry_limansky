export type Value = number | string | { [ket: string]: any };

export class ListNode {

    public next: ListNode | null = null;
    public previous: ListNode | null = null;

    constructor(public value: Value) {
        this.value = value;
    }

}
