import { NearestObject } from "./type-nearestobject";

export interface ISpaceObject {
    name: string;
    size: number;
    distanceFromSun: number;
    getNearestObject(objects: NearestObject): void;
}
