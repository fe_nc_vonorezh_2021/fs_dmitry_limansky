import React, { useState } from 'react';
import { Pet } from '../pets/pet';
import PetInfo from './pet-info';

interface PetType {
    pet: Pet;
}
const CurrentPet: React.FC<PetType> = ({pet}) => {

    const [isVisible, changeVisibility] = useState<boolean>(false);

    const changeVis = () => {
        isVisible ? changeVisibility(false) : changeVisibility(true);
    }
    
    return (
       <li onClick={changeVis}> 
           {pet.name} is a {pet.type}
           { isVisible ?  <PetInfo petInfo={pet}/>  : null }
       </li>
    );
};

export default CurrentPet;