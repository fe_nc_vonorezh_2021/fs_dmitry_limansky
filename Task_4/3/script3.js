function getRandomInt(min, max) {
    return Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min))) + Math.ceil(min);
}

function arraySort(array, direction) {
    array.sort((a, b) => direction == "asc" ? a - b : b - a);
    return array;
}

let ourArray = [];
let ourArrayLength = getRandomInt(3, 10);
for (let i = 0; i < ourArrayLength; i++) {
    ourArray.push(getRandomInt(-100, 100));
}
//для нечетного элемента массива по последовательности, не по элементу (нулевой элемент четный)
let sum1 = ourArray.reduce((total, curr, index) => index % 2 ? total + curr ** 2 : total, 0);
//для нечетного элемента массива по значению (проверка каждого элемента на четность и нечетность)
let sum2 = ourArray.reduce((total, curr) => curr % 2 ? total + curr ** 2 : total, 0);

alert("Массив до сортировки " + ourArray);
alert("Массив после сортировки по убыванию " + arraySort(ourArray, "desc"));
alert("Массив после сортировки по возрастанию " + arraySort(ourArray, "asc"));
alert("Сумма квадратов нечетных элементов по порядку " + sum1);
alert("Сумма квадратов нечетных элементов по элементу " + sum2);