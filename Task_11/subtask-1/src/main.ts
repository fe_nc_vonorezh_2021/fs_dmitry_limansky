
import { getWeather } from "./scripts/get-weather";

const currentWeatherButton: HTMLElement = <HTMLElement>document.getElementById("show_weather");
const oneDayWeatherButton: HTMLElement = <HTMLElement>document.getElementById("three_days");

getWeather("current", "Воронеж");

currentWeatherButton.addEventListener("click", () => {
    getWeather("current", "");
})

oneDayWeatherButton.addEventListener("click", () => {
    getWeather("one day", "");
})
