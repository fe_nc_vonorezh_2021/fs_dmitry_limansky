import { PetType, PetSex } from "./enums";

export class Pet {

    constructor(public _id: number, public name: string, public type: string, public sex: string,
        public weight: number, public canFly: boolean, public isHidden: boolean) {
    }

}
