//КЛАСС НЕБЕСНЫХ ТЕЛ
class HevenlyBody {

    constructor(name, size, distanceFromSun) {
        this.name = name;
        this.size = size;
        this.distanceFromSun = distanceFromSun;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get size() {
        return this._size;
    }

    set size(value) {
        this._size = value;
    }

    get distanceFromSun() {
        return this._distanceFromSun;
    }

    set distanceFromSun(value) {
        return this._distanceFromSun = value;
    }

    getNearestObject(objects) {
        let min = 0;
        let objectName = "";
        for (let i = 0; i < objects.length; i++) {
            if (Math.abs(objects[i].distanceFromSun - this._distanceFromSun) != 0) {
                min = Math.abs(objects[i].distanceFromSun - this._distanceFromSun);
                objectName = objects[i].name;
                break;
            }
        }
        //в этом цикле получаем значение любого расстояния, отличного от 0, ибо если 0, то это наша планета. Можно ли как-то избавиться от первого цикла, чтобы получить минимальное значение, оличное от 0? 

        for (let i = 1; i < objects.length; i++) {
            if (Math.abs(objects[i].distanceFromSun - this._distanceFromSun) < min && Math.abs(objects[i].distanceFromSun - this._distanceFromSun) != 0) {
                min = Math.abs(objects[i].distanceFromSun - this._distanceFromSun);
                objectName = objects[i].name;
            }
        }
        console.log("Блиэайшее небесное тело - " + objectName + "\nОно находится в " + min + " км");
    }
}

//КЛАСС ПЛАНЕТ
class Planet extends HevenlyBody {
    constructor(name, size, distanceFromSun, hasWater, alive) {
        super(name, size, distanceFromSun);
        this.hasWater = hasWater;
        this.alive = alive;
    }

    get hasWater() {
        return this._hasWater;
    }

    set hasWater(value) {
        this._hasWater = value;
    }

    get alive() {
        return this._alive;
    }

    set alive(value) {
        this._alive = value;
    }

    populateAliens() {
        if (this._hasWater) {
            console.log("Планета заселена инопланетянами!");
            this._alive = true;
        } else {
            console.log("Отсутствует вода на планете, заселение невозможно :( ");
        }

    }
}

//КЛАСС ЗВЕЗД
class Star extends HevenlyBody {
    constructor(name, size, distanceFromSun, brightness) {
        super(name, size, distanceFromSun);
        this.brightness = brightness;
    }

    get brightness() {
        return this._brightness;
    }

    set brightness(value) {
        this._brightness = value;
    }
}

//КЛАСС ГАЛАКТИК
class Galaxy {

    constructor(name, planetList, starList) {
        this.name = name;
        this.planetList = planetList;
        this.starList = starList;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get planetList() {
        return this._planetList;
    }

    set planetList(value) {
        this._planetList = value;
    }

    get starList() {
        return this._starList;
    }

    set starList(value) {
        return this._startList;
    }

    isAlive() {
        let isAlive = false;
        this._planetList.forEach(element => {
            if (element.alive) {
                isAlive = true;
                return;
            }
        });
        isAlive ? console.log("Данная галактика обитаема, берегись инопланетян!") : console.log("Данная галактика необитаема, инопланетян нет, но скоро появятся :(");
    }
}

let earth = new Planet("Earth", 10000, 25345, true, true);
let mars = new Planet("Mars", 23000, 19999, true, false);
let saturn = new Planet("Saturn", 142300, 199990, false, false);
let uranus = new Planet("Uranus", 232000, 1914999, false, false);

let alpha1 = new Star("Alpha 1", 33000, 24000);
let alpha2 = new Star("Alpha 2", 33000, 24000);
let alpha3 = new Star("Alpha 3", 33000, 24000);

let planets = [earth, mars, saturn, uranus];
let stars = [alpha1, alpha2, alpha3];

let milkyWay = new Galaxy("Milky Way", planets, stars);

saturn.populateAliens();
mars.populateAliens();
earth.getNearestObject(planets);
earth.getNearestObject(stars);
milkyWay.isAlive();