function changeVariables(firstVar, secondVar) {
    confirm('Использовать дополнительную переменную?') ? changeVariables1(firstVar, secondVar) :
        changeVariables2(firstVar, secondVar);
}

function changeVariables1(firstVar, secondVar) {
    alert(` До изменения: a = ${a}, b = ${b}`);
    let c = a;
    a = b;
    b = c;
    alert(`После изменения a = ${a}, b = ${b}`);
}

function changeVariables2(firstVar, secondVar) {
    alert(` До изменения: a = ${a}, b = ${b}`);
    //Используем деструктурирующее присваивание
    [a, b] = [b, a];
    alert(`После изменения a = ${a}, b = ${b}`);
}

let a = 2;
let b = 3;
changeVariables(a, b);
