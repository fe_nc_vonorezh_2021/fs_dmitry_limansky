const gulp = require("gulp");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");

gulp.task("scripts", function() {
  return gulp.src("../Task_4/**/*.js")
  .pipe(concat("mergedJs.js"))
  .pipe(uglify())
  .pipe(gulp.dest("./dist/"))
});
