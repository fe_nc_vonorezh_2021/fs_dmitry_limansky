let buttonAdd = document.getElementById("btnAdd");
let buttonDelete = document.getElementById("btnDel");
let inputField = document.getElementById("inputRow");
let mainTable = document.getElementById("table_1");
rowsEvent();

buttonAdd.onclick = () => {
    if (checkInpValue()) {
        let newRow = mainTable.insertRow(inputField.value);
        for (let i = 0; i < 4; i++) {
            newRow.insertCell(-1).innerHTML = "-";
        }
        rowsEvent();
    }
}

function checkInpValue() {
    return (inputField.value && Number(inputField.value) < mainTable.rows.length && Number(inputField.value) > 0);
}

buttonDelete.onclick = () => {
    if (mainTable.rows.length == 1) {
        return;
    } else if (checkInpValue()) {
        mainTable.deleteRow(inputField.value);
    }
}

function rowsEvent() {
    mainTable.addEventListener('click', function func(event) {
        let targerItem = event.target;
        if (targerItem.tagName != "TD") {
            return;
        }
        targerItem.setAttribute("contenteditable", true);
    });
}
