import { ListNode, Value } from "./node";

export class DoublyLinkedList {

    public head: ListNode | null = null;
    public tail: ListNode | null = null;
    public length: number = 0;

    private checkCorrectPosition(position: number): boolean {
        return position > this.length || position < 0;
    }

    public infoNode(position: number): ListNode | null {
        let currentNode: ListNode | null = this.head;
        let count: number = 0;
        if (this.length === 0 || this.checkCorrectPosition(position)) {
            console.log("Impossible to search node");
        } else {
            while (count++ < position) {
                currentNode = currentNode.next;
            }
            console.log(currentNode.value);
        }
        return currentNode;
    }

    public simpleAddNode(value: Value): void {
        let newNode: ListNode = new ListNode(value);
        if (this.head) {
            newNode.previous = this.tail;
            this.tail.next = newNode;
            this.tail = newNode;
        } else {
            this.head = newNode;
            this.tail = newNode;
        }
        this.length++;
        console.log("Node added");
    }

    public addNode(position: number, value: Value): void {
        if (this.checkCorrectPosition(position)) {
            console.log("Impossible to add element -> the position is incorrect");
            return;
        }
        let newNode: ListNode = new ListNode(value);
        if (this.length === 0 || position === this.length) {
            this.simpleAddNode(value);
        } else if (position === 0) {
            newNode.next = this.head;
            this.head.previous = newNode;
            this.head = newNode;
        } else {
            let currentNode: ListNode | null = this.head;
            let count: number = 0;
            while (count++ < position) {
                currentNode = currentNode?.next;
            }
            newNode.next = currentNode;
            newNode.previous = currentNode.previous;
            currentNode.previous.next = newNode;
            currentNode.previous = newNode;
        }
    }

    public edit(position: number, value: Value): void {
        if (this.checkCorrectPosition(position)) {
            console.log("Impossible to edit element -> the position is incorrect ");
            return;
        }
        if (this.head) {
            let currentNode = this.head;
            let count = 0;
            while (count++ < position) {
                currentNode = currentNode.next;
            }
            currentNode.value = value;
        }
    }

    public delete(position: number): void {
        if (this.checkCorrectPosition(position)) {
            console.log("Impossible to delete this element -> the position is incorrect");
            return;
        } else {
            if (this.length === 1) {
                this.head = null;
                this.tail = null;
            } else {
                if (position === 0) {
                    this.head.next.previous = null;
                    this.head = this.head.next;
                } else if (position === this.length) {
                    this.tail.previous.next = null;
                    this.tail = this.tail.previous;
                } else {
                    let count: number = 0;
                    let currentNode: ListNode = this.head;
                    while (count++ < position) {
                        currentNode = currentNode.next;
                    }
                    currentNode.previous.next = currentNode.next;
                    currentNode.next.previous = currentNode.previous;
                    currentNode.previous = currentNode.next = null;
                }
            }
        }
    }
}
