import React, { FC } from 'react';
import { Pet } from '../pets/pet';
import '../styles/pet-info.css';

interface PetType {
    petInfo: Pet
}

const PetInfo: FC<PetType> = ({ petInfo }) => {
    return (
        <div className="info_text">
            Its weight = {petInfo.weight}kg, male - {petInfo.sex}
            {petInfo.canFly
                ? <p> {petInfo.name} also can fly!</p>
                : null
            }
        </div>
    );
};

export default PetInfo;