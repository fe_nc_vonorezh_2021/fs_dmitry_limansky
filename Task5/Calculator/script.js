let inputField = document.querySelector('.current-operand');
let operation = "";
let memoryOperation = "";
let firstMemoryOperand = "";
let secondMemoryOperand = "";
//Using a closure
let closureResult = closure();
closureResult();

function input(inputNumb) {
    if (operation === "") {
        if((inputField.textContent.includes(".") && inputNumb === ".")){
            return;
        }
        inputField.textContent += inputNumb;
    }
     else {
        inputField.textContent = '';
        memoryOperation = operation;
        operation = "";
        inputField.textContent += inputNumb;
    }
}

//idk how to use clouseres in my script
function closure() {
    let closureLet = 10;
    const clouserTest = () => {
        console.log(closureLet);
    }
    closureLet++;
    return clouserTest;
}

const allClear = () => {
    inputField.textContent = "";
    operation = "";
    memoryOperation = "";
}

const deleteCharacter = () => {
    inputField.textContent = 0 ? inputField.textContent = "0" :
        inputField.textContent.substr(0, inputField.textContent.length - 1);
}

function standartOperation(oper) {
    operation = oper;
    if (memoryOperation) {
        calculate();      
    }
    else {
        firstMemoryOperand = inputField.textContent;
    }
}

function sqrtOperation(){
    calculate();
    memoryOperation = "sqrt";
    calculate();
}

function calculate() {
    secondMemoryOperand = inputField.textContent;
    switch (memoryOperation) {
        case "+":
            inputField.textContent = +firstMemoryOperand + +secondMemoryOperand;
            break;
        case "-":
            inputField.textContent = firstMemoryOperand - secondMemoryOperand;
            break;
        case "*":
            inputField.textContent = firstMemoryOperand * secondMemoryOperand;
            break;
        case "/":
            inputField.textContent = firstMemoryOperand / secondMemoryOperand;
            break;
        case "^":
            inputField.textContent = firstMemoryOperand ** secondMemoryOperand;
            break;
        case "sqrt": {
            inputField.textContent = String(Math.sqrt(firstMemoryOperand)).length > 14 ? (Math.sqrt(firstMemoryOperand)).toFixed(13) :
            (Math.sqrt(firstMemoryOperand));
        }      
        default:
            console.log('Operation problem');
    }

    memoryOperation = "";
    firstMemoryOperand = inputField.textContent;
}