export enum PetType {
    Cat = "cat",
    Dog = "dog",
    Mouse = "mouse",
    Rabbit = "rabbit",
    Hamster = "humster",
    Parrot = "parrot"
};

export enum PetSex {
    Male = "male",
    Female = "female"
};
