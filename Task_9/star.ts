import { HevenlyBody } from "./hevenlybody";

export class Star extends HevenlyBody {
    constructor(public name: string, public size: number, public distanceFromSun: number, public brightness: number) {
        super(name, size, distanceFromSun);
        this.brightness = brightness;
    }

}