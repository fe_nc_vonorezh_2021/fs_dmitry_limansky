import { Injectable } from '@angular/core';
import { filter } from 'rxjs';
import { PetType } from '../pets/enums';
import { Pet } from '../pets/pet';
import { petList } from '../pets/pets-list';

@Injectable({
  providedIn: 'root'
})

export class PetService {
  
  getPats() : Pet[] {
    return petList;
  }

  filetrPets() : Pet[] {
    return petList.filter( pet => pet.type != PetType.Cat);
  }
}
