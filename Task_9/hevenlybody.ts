import { ISpaceObject } from "./ispaceobject";
import { NearestObject } from "./type-nearestobject";

export class HevenlyBody implements ISpaceObject {

    public name: string;
    public size: number;
    public distanceFromSun: number;

    constructor(name: string, size: number, distanceFromSun: number) {
        this.name = name;
        this.size = size;
        this.distanceFromSun = distanceFromSun;
    }

    public getNearestObject(objects: NearestObject): void {
        let min: number = 0;
        let objectName: string = "";
        for (let i: number = 0; i < objects.length; i++) {
            if (Math.abs(objects[i].distanceFromSun - this.distanceFromSun) != 0) {
                min = Math.abs(objects[i].distanceFromSun - this.distanceFromSun);
                objectName = objects[i].name;
                break;
            }
        }

        for (let i: number = 1; i < objects.length; i++) {
            if (Math.abs(objects[i].distanceFromSun - this.distanceFromSun) < min && Math.abs(objects[i].distanceFromSun - this.distanceFromSun) != 0) {
                min = Math.abs(objects[i].distanceFromSun - this.distanceFromSun);
                objectName = objects[i].name;
            }
        }
        console.log("Блиэайшее небесное тело - " + objectName + "\nОно находится в " + min + " км");
    }
}