import React, { FC } from 'react';
import Header from './components/header';
import PetList from './components/pet-list';
import { Pet } from './pets/pet';
import { AppService } from './service/service';
import { useState } from 'react'
import './styles/app.css';
import { useTheme } from './hooks/use-theme';
import Button from './components/UI/button/button';

const service: AppService = new AppService();
const petList: Pet[] = service.getPetList();

const App: FC = () => {
  const {theme, setTheme} = useTheme();
  const [pets, setPets] = useState<Pet[]>(petList);
  
  const getCurrentPets = (status: string) => {
    status === "Hide cats" ? setPets(pets.filter( pet =>  pet.type !== "Cat")) : setPets(petList);
  }

  const changeTheme = () => {
    theme === 'light' ? setTheme('dark') : setTheme('light');
  }

  return (
   <div className='wrapper'>
    <Header/>
    <Button onClick={changeTheme}>Сhange theme</Button>
    <PetList pets={pets} getPets={getCurrentPets}/> 
   </div>
  );
}

export default App;
