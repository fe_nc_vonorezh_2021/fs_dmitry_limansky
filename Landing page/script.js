let inputForms = document.querySelectorAll("input");
console.log(inputForms);
let sendButton = document.getElementById("buttonSend");
let inputText;
let inputFields = {
    inputFirstName: ["", false],
    inputSecondName: ["", false],
    inputEmail: ["", false],
    inputPhone: ["", false],
    inputMessage: ["", false]
};
for (let inputs of inputForms) {
    inputs.onclick = function () {
        textValidation(inputs.id);
    }
}

setInputFromLS();

function setInputFromLS() {
    if (getCookie("check")) {
        return;
    }
    inputForms[0].value = inputFields.inputFirstName[0] = !!localStorage.firstNameLS ? localStorage.firstNameLS : "";
    inputForms[1].value = inputFields.inputSecondName[0] = !!localStorage.secondNameLS ? localStorage.secondNameLS : "";
    inputForms[2].value = inputFields.inputEmail[0] = !!localStorage.emailLS ? localStorage.emailLS : "";
    inputForms[3].value = inputFields.inputPhone[0] = !!localStorage.phoneNameLS ? localStorage.phoneNameLS : "";
    inputForms[4].value = inputFields.inputMessage[0] = !!localStorage.messageLS ? localStorage.messageLS : "";
}

function textValidation(input) {

    let inputFormById = document.getElementById(input);
    inputText = inputFormById.value.trim();
    let check = false;

    inputFormById.onblur = function () {
        let firstMap = new Map([
            ["firstName", [inputFields.inputFirstName, standartValidation(inputText), "firstNameLS"]],
            ["secondName", [inputFields.inputSecondName, standartValidation(inputText), "secondNameLS"]],
            ["textMessage", [inputFields.inputMessage, standartValidation(inputText), "messageLS"]],
            ["emailAddress", [inputFields.inputEmail, emailValidation(inputText), "emailLS"]],
            ["phoneNumb", [inputFields.inputPhone, phoneValidation(inputText), "phoneNameLS"]]
        ]);
        let mapResult = firstMap.get(input);
        let inputField = mapResult[0];
        check = mapResult[1];
        inputField[0] = inputText;
        inputField[1] = Boolean(check);
        localStorage.setItem(mapResult[2], inputText);

        if (check) {
            inputFormById.parentElement.style.border = " 2px solid green";
            inputFormById.parentElement.style.borderRadius = "12px";
            console.log("Validation complete!");
        } else {
            inputFormById.parentElement.style.border = " 2px solid red";
            inputFormById.parentElement.style.borderRadius = "12px";
        }
        return check;
    }
}

function standartValidation(input) {
    return (input && (/\S/).test(input));
}

function emailValidation(input) {
    return (input && (/[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+/).test(input));
}

function phoneValidation(input) {
    return (input === "" || (/^\+\d{1}\(\d{3}\)\d{2}-\d{2}-\d{3}$/).test(input));
}

function clearInputs() {
    for (let i = 0; i < inputForms.length; i++) {
        inputForms[i].value = "";
        inputForms[i].parentElement.style.border = " 2px solid white";
    }
}

sendButton.onclick = function () {

    console.log("Функция сработала");
    console.log(getCookie("name"));

    if (getCookie("check")) {
        alert(getCookie("name") + " " + getCookie("surname") + ", Ваше сообщение обрабатывается!");
        clearInputs();
    } else if (standartValidation(inputFields.inputFirstName[0]) && standartValidation(inputFields.inputSecondName[0]) &&
        emailValidation(inputFields.inputEmail[0]) && phoneValidation(inputFields.inputPhone[0]) &&
        standartValidation(inputFields.inputMessage[0])) {
        alert(inputFields.inputFirstName[0] + " " + inputFields.inputSecondName[0] + ", спасибо за обращение!");
        setCookie("name", inputFields.inputFirstName[0], {
            secure: true,
            'max-age': 3600
        });
        setCookie("surname", inputFields.inputSecondName[0], {
            secure: true,
            'max-age': 3600
        });
        setCookie("check", "true", {
            secure: true,
            'max-age': 3600
        });
        clearInputs();
    } else {
        alert("Некоторые поля некорректны!");
    }
}

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {

    options = {
        path: '/',
    };

    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}
