function getRandomInt(min, max) {
    return Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min))) + Math.ceil(min);
}

function startGame() {
    let unknownNumber = Number(getRandomInt(1, 1000));
    let numberOfAttempts = 0;
    let message = "Введите ваше число: ";
    let yourNumber = -1;
    do {
        yourNumber = Number(prompt(message, ''));
        alert(typeof (yourNumber));
        numberOfAttempts++;
        if (isNaN(yourNumber) || yourNumber === 0) {
            message = "Ошибка ввода, введите число!";
            continue;
        }
        message = (yourNumber < unknownNumber) ? "Твое число меньше задуманного" :
            yourNumber > unknownNumber ? "Твое число больше задуманного" :
            message = "Ошибка ввода. Введите число!";
    }
    while (unknownNumber != yourNumber);
    if (confirm("Вы угадали число " + yourNumber + ", количество попыток = " +
            numberOfAttempts + "\nНачать иигру заново?")) {
        startGame();
    }
}
