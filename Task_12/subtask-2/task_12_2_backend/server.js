const express = require("express");
const MongoClient = require("mongodb").MongoClient;
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const port = 3000;
const url = 'mongodb+srv://woodu:TwAwcQwPcQKC3RlW@woodufx.jczm8.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

app.use(cors());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

MongoClient.connect(url, (err, database) => {
  if (err) {
    return console.log(err);
  }
  app.post("/animals", (req, res) => {
    var id = Math.floor(Math.random(20) * 1000);
    const animal = {
      _id: id,
      name: req.body.name,
      type: req.body.type,
      sex: req.body.sex,
      canFly: req.body.canFly,
      isHidden: req.body.isHidden,
      weight: req.body.weight,
    };
    db.collection("AnimalsCollection")
      .insertOne(animal, (err, animal) => {
        if (err) {
          res.send({ error: "An error has occurred" });
        } else {
          res.send(animal);
        }
      });
  });

  app.get("/animals", (req, res) => {
    db = database.db("Animals");
    db.collection("AnimalsCollection")
      .find()
      .toArray(function (err, animal) {
        if (err) {
          return console.log(err);
        }
        res.send(animal);
      });
  });

  app.get("/animals/:id", (req, res) => {
    const id = parseInt(req.params.id);
    db.collection("AnimalsCollection")
      .findOne({ _id: id }, (err, animal) => {
        if (err) {
          return console.log(err);
        } else {
          res.send(animal);
        }
      });
  });

  app.delete("/animals/:id", (req, res) => {
    const id = parseInt(req.params.id);
    db.collection("AnimalsCollection")
      .deleteOne({ _id: id }, (err, animal) => {
        if (err) {
          return console.log(err);
        }
        res.send(animal);
      });

  });

  app.put("/animals/:id", (req, res) => {
    let id = parseInt(req.params.id);
    const animal = {
      _id: id,
      name: req.body.name,
      type: req.body.type,
      sex: req.body.sex,
      weight: req.body.weight,
    };
    db.collection("AnimalsCollection")
      .replaceOne({ _id: id }, animal, (err, animal) => {
        if (err) {
          return console.log(err);
        }
        res.send(animal);
      });
  });


  app.listen(port, () => {
    console.log("port: " + port);
  });

});
