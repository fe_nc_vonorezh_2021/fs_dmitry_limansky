| Date     | Name        | Description                        | Link |
|----------|-------------|------------------------------------|------|
| 13.10.21 | First task | Created readme and changelog files | https://gitlab.com/fe_nc_vonorezh_2021/fs_dmitry_limansky/-/merge_requests/1   |
| 21.10.21 | Second task | Created the site page (css+html) | https://gitlab.com/fe_nc_vonorezh_2021/fs_dmitry_limansky/-/merge_requests/2   |
| 27.10.21 | Fourth task | Completed task_4|https://gitlab.com/fe_nc_vonorezh_2021/fs_dmitry_limansky/-/merge_requests/3|
| 06.11.21 | Fifth task | Completed OOP task|https://gitlab.com/fe_nc_vonorezh_2021/fs_dmitry_limansky/-/merge_requests/5|
| 10.11.21 | Sixth task | Completed task_6|https://gitlab.com/fe_nc_vonorezh_2021/fs_dmitry_limansky/-/merge_requests/6|
| 12.11.21 | Seventh task| Completed task 7|https://gitlab.com/fe_nc_vonorezh_2021/fs_dmitry_limansky/-/merge_requests/7|
| 07.11.21 | Eighth task |Gulp and Webpack|https://gitlab.com/fe_nc_vonorezh_2021/fs_dmitry_limansky/-/merge_requests/8|
| 30.11.21 | Ninth task | Completed TS task|https://gitlab.com/fe_nc_vonorezh_2021/fs_dmitry_limansky/-/merge_requests/9|
| 10.12.21 | Tenth-1 task |Doubly-list and qsort|https://gitlab.com/fe_nc_vonorezh_2021/fs_dmitry_limansky/-/merge_requests/10|
| 22.12.21 | Task-10-2 |RXJS|https://gitlab.com/fe_nc_vonorezh_2021/fs_dmitry_limansky/-/merge_requests/11|
