export class Factory {
    @logClassName
    create<T>(type: { new(...args): T }, ...args): T {
        return new type(...args);
    }
}

interface PropertyDescriptor{
    configurable?: boolean;
    enumerable?: boolean;
    value?: any;
    writable?: boolean;
    get? (): any;
    set? (v: any): void;
}

function logClassName(target: Object, key: string, param: PropertyDescriptor): void {
    const result = param.value;
    param.value = function (...args: any[]): any {
        console.log("Object " + args[0].name + " created!");
        return result.apply(this, args);
    }
}
