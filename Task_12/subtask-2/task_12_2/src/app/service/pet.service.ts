import { Injectable } from '@angular/core';
import { PetType } from '../pets/enums';
import { Pet } from '../pets/pet';
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})

export class PetService {

  constructor(private http: HttpClient) {
  }

  public getPets(): Observable<Pet[]> {
    return this.http.get<Pet[]>("http://localhost:3000/animals")
      .pipe(map((res: Pet[]) => {
        return res;
      }));
  }

  public getPet(id: number): Observable<Pet> {
    return this.http.get<Pet>(`http://localhost:3000/animals/${id}`)
      .pipe(map((res: Pet) => {
        return res;
      }));
  }

  public updatePet(data: Pet, id: number): Observable<Pet> {
    return this.http.put<Pet>("http://localhost:3000/animals/" + id, data)
      .pipe(map((res: Pet) => {
        return res;
      }));
  }

  public insertPet(data: Pet): Observable<Pet> {
    return this.http.post<Pet>("http://localhost:3000/animals", data)
      .pipe(map((res: Pet) => {
        return res;
      }));
  }

  public deletePet(id: number): Observable<Pet> {
    return this.http.delete<Pet>("http://localhost:3000/animals/" + id)
      .pipe(map((res: Pet) => {
        return res;
      }));
  }
}
