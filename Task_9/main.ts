import { Factory } from "./factory";
import { Planet } from "./planet";
import { Star } from "./star";
import { Galaxy } from "./galaxy";

const factory: Factory = new Factory();
factory.create(Planet, "Earth", 10000, 25345, true, true);

const earth: Planet = factory.create(Planet, "Earth", 10000, 25345, true, true);
const mars: Planet = factory.create(Planet, "Mars", 23000, 19999, true, false);
const saturn: Planet = factory.create(Planet, "Saturn", 142300, 199990, false, false);
const uranus: Planet = factory.create(Planet, "Uranus", 232000, 1914999, false, false);

const alpha1: Star = factory.create(Star, "Alpha 1", 33000, 24000, 213);
const alpha2: Star = factory.create(Star, "Alpha 2", 33000, 24000, 321);
const alpha3: Star = factory.create(Star, "Alpha 3", 33000, 24000, 542);

const planets: Planet[] = [earth, mars, saturn, uranus];
const stars: Star[] = [alpha1, alpha2, alpha3];

const milkyWay: Galaxy = factory.create(Galaxy, "Milky Way", planets, stars);

saturn.populateAliens();
mars.populateAliens();
earth.getNearestObject(planets);
earth.getNearestObject(stars);
milkyWay.isAlive();
