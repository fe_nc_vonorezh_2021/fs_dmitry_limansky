import { fivthBlock, fourthBlock, sixthBlock } from "./const";

export function insertWeatherForThreeDays(hourlyData): void {
    fourthBlock.style.display = "none";
    fivthBlock.style.display = "none";
    sixthBlock.style.display = "none";
    let counter: number = 0;
    while (counter < 3) {
        switch (counter) {
            case 0:
                document.getElementById(`pred_${counter + 1}_id`).innerHTML = "Сегодня";
                break;
            case 1:
                document.getElementById(`pred_${counter + 1}_id`).innerHTML = "Завтра";
                break;
            case 2:
                document.getElementById(`pred_${counter + 1}_id`).innerHTML = "Послезавтра";
                break;
        }
        document.getElementById(`deg_${counter + 1}_id`).innerHTML = String(Math.round(hourlyData.daily[counter].temp.day - 273)) + "°";
        document.getElementById(`img_${counter + 1}_id`).innerHTML = `<img src="http://openweathermap.org/img/wn/${hourlyData.daily[counter].weather[0].icon}@4x.png" alt=""></div>`;
        counter++;
    }
}