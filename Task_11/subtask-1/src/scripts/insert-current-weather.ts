import { fivthBlock, fourthBlock, sixthBlock } from "./const";
export function insertCurrentWeather(hourlyData): void {
    const Data: Date = new Date();
    let hour: number = Number(Data.getHours());
    let counter: number = 1;
    fourthBlock.style.display = "block";
    fivthBlock.style.display = "block";
    sixthBlock.style.display = "block";

    while (counter < 7) {
        let hourHeader: string = (hour / 10 >= 1 && hour <= 23) ? String(hour) : "0" + String(hour % 24);
        if (counter === 1) {
            hourHeader = "Сейчас";
        }
        document.getElementById(`pred_${counter}_id`).innerHTML = hourHeader;
        document.getElementById(`deg_${counter}_id`).innerHTML = String(Math.round(hourlyData.hourly[counter - 1].temp - 273)) + "°";
        document.getElementById(`img_${counter}_id`).innerHTML = `<img src="http://openweathermap.org/img/wn/${hourlyData.hourly[counter - 1].weather[0].icon}@4x.png" alt=""></div>`;
        hour++;
        counter++
    }
}