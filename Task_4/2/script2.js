let userName;
let userAge;

do {
  userName = prompt("Введите ваше имя:", "");
}
while (userName === '');

do {
  userAge = prompt("Введите ваш возраст:", "");
  if (userAge < 0 || userAge === '') {
    alert("Введите данные корректно!");
  }
}
while (userAge < 0 || userAge === "");

let correctUserName = userName.replace(userName[0], userName[0].toUpperCase());
alert(` Привет, ${correctUserName}, тебе уже ${userAge} `);
