export function insertInformationBlock(data): void {
    document.getElementById("city_name").innerHTML = data.name;
    document.getElementById("city_weather").innerHTML = data.weather[0].description;
    document.getElementById("city_degrees").innerHTML = String(Math.round(data.main.temp - 273)) + "°";
    document.getElementById("feeling_id").innerHTML = String(Math.round(data.main.feels_like - 273)) + "°";
    document.getElementById("preasuer_id").innerHTML = String(Math.round(data.main.pressure * 0.750064)) + "мм рт ст";
    document.getElementById("humidity_id").innerHTML = String(data.main.humidity) + "%";
    document.getElementById("wind_id").innerHTML = String(data.wind.speed) + "м/c";
}
