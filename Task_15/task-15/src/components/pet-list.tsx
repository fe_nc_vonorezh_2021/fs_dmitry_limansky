import React, {FC, useState } from 'react';
import { Pet } from '../pets/pet';
import CurrentPet from './current-pet';
import '../styles/pet-list.css'
import Button from './UI/button/button';

interface PetsType {
    pets: Pet[];
    getPets(str: string): void;
}

const PetList: FC<PetsType> = ({pets, getPets}) => {

    const [btnText, changeText] = useState<string>("Hide cats");

    const hideCats = () => {
        getPets(btnText);
        btnText === "Hide cats" ? changeText("Show cats") : changeText("Hide cats");
    }

    return (
        <div>
            <div className="petList">
                <Button onClick={hideCats}> {btnText} </Button>
                <ul className="border">
                    {pets.map((pet: any) => 
                        <CurrentPet pet={pet} key={pet.id}/>
                    )}
                </ul>
            </div>
        </div>
    );
};

export default PetList;