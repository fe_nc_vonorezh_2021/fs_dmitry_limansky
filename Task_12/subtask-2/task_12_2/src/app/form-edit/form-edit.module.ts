import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormEditComponent } from './form-edit.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [FormEditComponent],
  imports: [
    CommonModule, ReactiveFormsModule
  ],
  exports: [FormEditComponent]
})
export class FormEditModule { }
