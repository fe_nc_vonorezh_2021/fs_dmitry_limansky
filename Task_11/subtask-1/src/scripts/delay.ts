import { errorMessage } from "./const";

export function delayMessage(): void {
    errorMessage.style.display = "block";
    setTimeout(() => errorMessage.style.display = "none", 6000);
}