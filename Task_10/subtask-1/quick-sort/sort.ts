const array: number[] = [4, 12, 43, 12, 33, 42, 53, 1, 0, -4, 12, -5, 100, -35];

function quickSort(arr: number[]): number[] {
    if (arr.length < 2) {
        return arr;
    } else {
        const pivot: number = arr[Math.floor(Math.random() * arr.length)];
        const left: number[] = arr.filter(value => value < pivot);
        const right: number[] = arr.filter(value => value > pivot);
        return [...quickSort(left), pivot, ...quickSort(right)];
    }
}

console.log(`Array before quicksort: ${array}`);
console.log(`Array after quicksort ${quickSort(array)}`);
