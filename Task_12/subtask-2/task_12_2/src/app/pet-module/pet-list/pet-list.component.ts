import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Pet } from 'src/app/pets/pet';
import { PetService } from 'src/app/service/pet.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styleUrls: ['./pet-list.component.less']
})
export class PetListComponent implements OnInit {
 
  public pets: Pet[];
  public buttonText: string = "Hide cats";
  public hideCats: boolean = false;

  constructor(private service: PetService, private router: Router) { };

   public showOrHideCats(): void {
    this.hideCats = this.hideCats ? false : true;
    this.buttonText = this.hideCats ? "Show cats" : "Hide cats";
    // this.hideCats ? this.getFilteredPets(): this.getPets();
  } 
  
  public ngOnInit(): void {
    this.getPets();
    console.log("init");
  }

  private getPets(): void {
    this.service.getPets()
      .subscribe(res => {
        this.pets = res;
      });
  }

  private getFilteredPets(): void {
    this.pets = this.pets.filter(pet => pet.type !== "cat");
  }

  public deleteThisPet(pet: Pet) : void {
    if(confirm("Are you sure to delete "+ pet.name)) {
      this.service.deletePet(pet._id)
      .subscribe(res => {
        this.getPets();
      });
    }
  }

  public consoleSay(name: string) {
    console.log(`${name} said "${name} is a fool!"`)
  }
  
  public goToEdit(id: number) {
    this.router.navigate(['/pet', id]);
    console.log(id);
  }

  public goToAdd(): void {
    this.router.navigate(["addForm"]);
  }
}
