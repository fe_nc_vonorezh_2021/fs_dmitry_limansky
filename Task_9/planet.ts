import { HevenlyBody } from "./hevenlybody";

export class Planet extends HevenlyBody {
    private hasWater: boolean;
    private alive: boolean;
    constructor(name: string, size: number, distanceFromSun: number, hasWater: boolean, alive: boolean) {
        super(name, size, distanceFromSun);
        this.hasWater = hasWater;
        this.alive = alive;
    }

    public getHasWater(): boolean {
        return this.hasWater;
    }

    public setHasWater(value: boolean): boolean {
        return this.hasWater = value;
    }

    public getAlive(): boolean {
        return this.alive;
    }

    public setAlive(value: boolean): boolean {
        return this.alive = value;
    }

    public populateAliens(): void {
        if (this.hasWater) {
            console.log("Планета заселена инопланетянами!");
            this.alive = true;
        } else {
            console.log("Отсутствует вода на планете, заселение невозможно :( ");
        }

    }
}